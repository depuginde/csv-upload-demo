public class CSV_Upload {
    public List<List<String>> fileLines {get; set;} 
    public String nameFile{get;set;}
    public  Blob contentFile{get;set;}
    public String ErrorMessage {get;set;}
	
    public CSV_Upload()
    {
      fileLines  = new List<List<String>>();
        
    }
    
    public void readFile()
    {
        try
        {
            nameFile = CSV_Parser.blobToString(contentFile);
        }
        Catch(System.StringException stringException)
        {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.Upload_Case_String_Exception_Error + stringException);
            ApexPages.addMessage(errormsg);    
        }
        CSV_Parser CSV_ParserInst = new CSV_Parser();
        
        try
        {
        	filelines = CSV_ParserInst.parseCSV(nameFile, false);
        }
        Catch(System.Exception exp)
        {
           
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,'Exception' + exp);
            ApexPages.addMessage(errormsg);    
        }
    }


}
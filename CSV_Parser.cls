public class CSV_Parser 
{
    /** 
        Method Name  : parseCSV
        Return Type  : String contents,Boolean skipHeaders
        Description  : Business logic for parsing the string         
    */
    public List<List<String>> parseCSV(String contents,Boolean skipHeaders) 
    {
        List<List<String>> allFields = new List<List<String>>();
    
        // replace instances where a double quote begins a field containing a comma
        // in this case you get a double quote followed by a doubled double quote
        // do this for beginning and end of a field
        contents = contents.replaceAll(',"""',',"DBLQT').replaceall('""",','DBLQT",');
        // now replace all remaining double quotes - we do this so that we can reconstruct
        // fields with commas inside assuming they begin and end with a double quote
        contents = contents.replaceAll('""','DBLQT');
        // we are not attempting to handle fields with a newline inside of them
        // so, split on newline to get the spreadsheet rows
        List<String> lines = new List<String>();
        try {
            lines = contents.split('\r\n');
        } catch (System.ListException e) {
            System.debug('Limits exceeded?' + e.getMessage());
        }
        system.debug(lines+'lines');
        Integer num = 0;
        for(String line : lines) {
            system.debug('line'+line);
            // check for blank CSV lines (only commas)
            if (line.replaceAll(',','').trim().length() == 0) continue;
            
             List<String> fields = line.split(','); 
            system.debug(fields);
            List<String> cleanFields = new List<String>();
            String compositeField;
            Boolean makeCompositeField = false;
            for(String field : fields) {
                if (field.startsWith('"') && field.endsWith('"')) {
                    cleanFields.add(field.replaceAll('DBLQT','"'));
                } else if (field.startsWith('"')) {
                    makeCompositeField = true;
                    compositeField = field;
                } else if (field.endsWith('"')) {
                    compositeField += ',' + field;
                    cleanFields.add(compositeField.replaceAll('DBLQT','"'));
                    makeCompositeField = false;
                } else if (makeCompositeField) {
                    compositeField +=  ',' + field;
                } else {
                    cleanFields.add(field.replaceAll('DBLQT','"'));
                }
            }
            
            allFields.add(cleanFields);
        }
        system.debug(allFields.size()+'allFields');
        system.debug(allFields+'allFields');
        if (skipHeaders) allFields.remove(0);
        system.debug(allFields+'allFields');
        system.debug(allFields.size()+'allFields');
        return allFields;       
    }
    
    public static String blobToString(Blob input)
	{
        String hex = EncodingUtil.convertToHex(input);
        //System.assertEquals(0, hex.length() & 1);
        final Integer bytesCount = hex.length() >> 1;
        String[] bytes = new String[bytesCount];
        for(Integer i = 0; i < bytesCount; ++i)
            bytes[i] =  hex.mid(i << 1, 2);
        return EncodingUtil.urlDecode('%' + String.join(bytes, '%'), 'UTF-8');
	}

}